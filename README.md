# MQTT to Redis Client

### Introduction
mqttToRedis client is simple Python script used to subscribe to MQTT Broker and topic(s) of interest. When messages are published to subscribed topic(s), script will receive them and save to Redis in-memory database.

### Prerequisites
Redis is installed in the same host with the mqttToRedis client.


