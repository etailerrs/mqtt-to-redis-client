# MQTT Broker parameters
mqttServer="MQTT_HOST"
mqttPort="1883"

# MQTT Topic to Subscribe
channelSubs="MQTT_TOPIC"

# Import LIBs
import paho.mqtt.client as mqtt
from datetime import datetime
from redis import Redis

# The callback function to execute when connected to MQTT Broker
def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))
    client.subscribe(channelSubs)

# The callback function to execute when message is received from the server
def on_message(client, userdata, msg):
    t = datetime.now();
    ts = t.strftime("%Y-%m-%d_%H:%M:%S")
    print(msg.topic+":" + ts + " "+str(msg.payload))

    # Write to Redis DB
    redis.hset(msg.topic + ":" + ts, "temp", msg.payload)

# Create Redis connection on localhost
redis = Redis()

# Connect to MQTT Broker
client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
client.connect(mqttServer,mqttPort, 60)

# Wait in the loop
client.loop_forever()






